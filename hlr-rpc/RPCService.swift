//
//  RPCService.swift
//  hlr-rpc
//
//  Created by Fredrik on 02/02/2017.
//  Copyright © 2017 Hailer. All rights reserved.
//

import Foundation
import SwiftyJSON
import SocketIO
import MulticastDelegateSwift

public protocol RPCServiceDelegate: class {
    func socketConnected()
    func socketDisconnected()
    func socketSignalReceived(_ signal: String, meta: JSON)
    func socketReconnect()
    func socketReconnectAttempt()
}

public class RPCService {
    typealias socketCallback = (_ err: Bool, _ data: JSON) -> Void
    typealias socketSubscriber = (_ meta: JSON) -> Void
    
    private var id: Int = 0;
    private var cbs = [Int:socketCallback]()
    private var subscribers = Dictionary<String, socketSubscriber>()
    private var hasSubscribe: Bool = false
    
    static let sharedInstance: RPCService = RPCService()
    
    public var delegate = MulticastDelegate<RPCServiceDelegate>()
    public var remoteHost: String = "https://dev.hailer.com"
    public var socket: SocketIOClient!
    public var socketConfig: SocketIOClientConfiguration!
    
    private init() { }
    
    public static var instance : RPCService {
        get { return sharedInstance }
    }
    
    public func setup(url: String, config: SocketIOClientConfiguration) {
        let manager = SocketManager(socketURL: URL(string: url)!, config: config)
        socket = manager.defaultSocket
        self.remoteHost = url
        self.socketConfig = config
        
        if socket.status == .connected || socket.status == .connecting {
            socket.disconnect()
        }
        self.addHandlers()
        socket.connect()
    
    }
    
    public func getStatus() -> SocketIOStatus {
        let status = socket.status
        return status
    }
    
    public func addHandlers() {
        
        self.socket.on(clientEvent: SocketClientEvent.connect) { data, ack in
            self.delegate |> { delegate in
                delegate.socketConnected()
            }
        }
        
        self.socket.on(clientEvent: SocketClientEvent.reconnectAttempt) { data, ack in
            self.hasSubscribe = false
            self.delegate |> { delegate in
                delegate.socketReconnectAttempt()
            }
        }
        
        self.socket.on(clientEvent: SocketClientEvent.reconnect) { data, ack in
            self.hasSubscribe = false
            self.delegate |> { delegate in
                delegate.socketReconnect()
            }
        }
        
        self.socket.on(clientEvent: SocketClientEvent.disconnect) { data, ack in
            self.hasSubscribe = false
            self.delegate |> { delegate in
                delegate.socketDisconnected()
            }
        }
        
        self.socket.on("rpc-response") {data, ack in
            var json = JSON(data);
            if let sig = json[0]["data"]["sig"].string {
                json[0]["data"]["ack"].string = sig
            }
            if let err = json[0]["data"]["err"].int {
                json[0]["data"]["ack"].int = err
            }
            if let ackId = json[0]["data"]["ack"].int {
                if let fn = self.cbs[ackId] {
                    fn(json[0]["data"]["err"].exists(), json[0]["data"]["data"])
                    if !json[0]["data"]["sig"].exists() {
                        self.cbs.removeValue(forKey: ackId)
                    }
                }
            } else if json[0]["data"]["data"]["sig"].exists() {
                let meta = json[0]["data"]["data"]["meta"]
                if let srcSignal = json[0]["data"]["data"]["sig"].string {
                    self.delegate |> { delegate in
                        delegate.socketSignalReceived(srcSignal, meta: meta)
                    }
                    if let signal = self.subscribers[srcSignal] {
                        signal(meta)
                    }
                }
            }

        }
    }
    
    public func returnSocket () -> SocketIOClient {
        return socket
    }
    
    public func isConnected () -> Bool {
        let status = socket.status
        return (status == .connected) ? true : false
    }
    
    public func disconnect () {
        socket.disconnect()
    }
    
    public func close () {
        socket.disconnect()
    }
    
    public func reconnect () {
//        socket.reconnect()
    }
    
    public func connect () {
        socket.connect()
    }
    
    public func rpc (_ op: String, args: [Any], cb: @escaping (_ err: Bool, _ data: JSON)->Void) {
        if self.isConnected() {
            self.id = self.id + 1
            let params: [String : Any] = [
                "op": op,
                "args": args,
                "id": self.id
                ]
            socket.emit("rpc-request", params)
            cbs[id] = cb
        } else {
            cb(true, "Cannot connect to Hailer.")
        }
    }
    
    public func subscribe (_ signal: String, cb: @escaping (_ meta: JSON)->Void) {
        let isSignalSubscribed = (self.subscribers[signal] != nil)
        if !isSignalSubscribed {
            self.subscribers[signal] = cb
        }
    }

    public func unsubscribeSignal (_ signal: String) {
        if let _ = self.subscribers[signal] {
            self.subscribers.removeValue(forKey: signal)
        }
    }

}
